import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';

// import UserContext from '../UserContext';

export default function Logout(){

	// const {user} = useContext(UserContext);

	useEffect(()=>{
		// console.log(user);
		localStorage.clear();

	},[])

	return <Navigate to={`/`}/>

}