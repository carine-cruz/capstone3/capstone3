import {Container, Row, Col, Button, Form} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom'
// import { useContext } from 'react';
// import UserContext from './../UserContext';

export default function Register(){

	//React hooks --------------------------------------------
	const [fN, setFn] = useState("");
	const [lN, setLn] = useState("");
	const [email, setEmail] = useState("");
	const [pw, setPw] = useState("");
	const [vPW, setVPW] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);
	const [isValid, setIsValid] = useState(true);
	const [isLoading, setIsLoading] = useState("noLoad");

	// const {user, setUser} = useContext(UserContext);

	const navigate = useNavigate();

	// let displayLoading

	//React useEffect------------------------------------------
	useEffect(()=>{

		if(localStorage.getItem('token')){
			navigate(`../products`);
		}

		//submit button enabled only if all fields are filled up
		if(fN !== "" && lN!=="" && email !== "" && pw !== "" && vPW !== "" && mobileNo !== ""){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[fN,lN,email,pw,vPW,mobileNo])

	//Functions and Events----------------------------------------------------

	//displayLoading
	const displayLoading = () => {
		
		if (isLoading === "load"){
			return "spinner-border spinner-border-sm mx-1";
		} else if (isLoading === "noLoad"){
			return null;
		}
	}

	//email check
	const validateEmail = (emailAddress) => {
		
		setIsLoading("load");
		
		fetch('https://tranquil-refuge-66470.herokuapp.com/api/users/email-exists', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			if (response){
				setIsValid(false);
				alert('Email already registered.');
			} else {
				setIsValid(true);
			}
			setIsLoading("noLoad");
		})
		
		

	}

	const validateEmailCSS = () => {
		if(isValid){
			return "login";
		} else {
			return "login loginFailed";
		}
	}

	//register user
	const registerUser = (e) =>{
		e.preventDefault();

		if (pw === vPW){
			fetch('https://tranquil-refuge-66470.herokuapp.com/api/users/user-registration', {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
					body: JSON.stringify({
						firstName: fN,
						lastName: lN,
						email: email,
						password: pw,
						mobileNo: mobileNo
					})
			})
			.then(response => response.json())
			.then(response => {

				if(response){
					alert('Registration successful.');
					navigate('/login');
				} else {
					alert('Something went wrong. Please try again.');
				}
			})
		} else {
			alert(`Passwords do not match`)
		}
	}


	return(
		<Container fluid className="welcome min-vh-100">
			{/*<h4 className="text-center p-3 mx-auto"><span className="logo">THE BAKERY</span></h4>*/}
			<div style={{height:70}}/>
			<Row className="my-auto loginRow">
				<Col className="my-auto loginMargin" md={3} xs={10}>
					<h1 className="loginLabel">REGISTER</h1>
					<p>Already registered? Log-in <a href="/login" id="loginLink">here</a></p>
					<Form onSubmit={(e) => registerUser(e)}>

						<Form.Group className="mb-3">
							<Form.Control 
								type="text" 
								placeholder="Enter first name" 
								className="login" 
								value={fN}
								onChange={(e)=> setFn(e.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Control 
								type="text" 
								placeholder="Enter last name" 
								className="login" 
								value={lN}
								onChange={(e)=> setLn(e.target.value)}
							/>
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Control 
								type="email" 
								placeholder="Enter email" 
								className={validateEmailCSS()}
								value={email}
								onChange={(e)=> setEmail(e.target.value)}
								onBlur={(e)=>validateEmail({email})}
							/>
							<span className={displayLoading()}></span>
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Control 
								type="number" 
								placeholder="Enter mobile number" 
								className="login"
								value={mobileNo}
								onChange={(e)=> setMobileNo(e.target.value)}
							/>
						</Form.Group>

					  	<Form.Group className="mb-3">
					    	<Form.Control 
					    		type="password" 
					    		placeholder="Password" 
					    		className="login"
					    		value={pw}
					    		onChange={(e)=> setPw(e.target.value)}
					    	/>
					  	</Form.Group>

					  	<Form.Group className="mb-3">
					  		<Form.Control 
					  			type="password" 
					  			placeholder="Verify password" 
					  			className="login"
					  			value={vPW}
					  			onChange={(e)=> setVPW(e.target.value)}
					  		/>
					  	</Form.Group>

					  	<center><Button variant="outline-light" type="submit" disabled={isDisabled} className="mainButton">
					    Submit
					  	</Button></center>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}