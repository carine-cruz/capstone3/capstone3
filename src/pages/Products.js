import { Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

import ProductCard from '../components/ProductCard';
import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';

const admin = localStorage.getItem('admin');
const firstName = localStorage.getItem('firstName');

export default function Products(){

	const [products, setProducts] = useState([]);
	const [breads, setBreads] = useState([]);
	const [cakes, setCakes] = useState([]);
	const [bites, setBites] = useState([]);
	const [cheesecakes, setCheesecakes] = useState([]);
	const [pies, setPies] = useState([]);

	//variables for pagination
	const [currentPage, setCurrentPage] = useState(0);
	const [itemsPerPage, setItemsPerPage] = useState(0);

	const {category} = useParams();
	const { state, dispatch } = useContext(UserContext);
	const navigate = useNavigate();

	const screenSize = window.innerWidth;

	//retrieve products upon page load
	useEffect(()=>{
		
		if (admin === "true"){
			dispatch({type:"ADMIN", name: firstName, admin: true});
		} else if (admin === "false") {
			dispatch({type:"USER", name: firstName, admin: false});
		}

		if (state.admin === true){
			navigate(`../all-products`);
		} else{
			fetchData();
		} 

		if (screenSize <= 1440){
			setItemsPerPage(4);
		} else if (screenSize > 1440 && screenSize < 2560 ){
			setItemsPerPage(8);
		} else {
			setItemsPerPage(16);
		}

	},[])

	const fetchData = () => {

		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/`)
		.then(result => result.json())
		.then(result => {

			let temp = [];

			
			result.forEach(product=>{
				if(product.category.toLowerCase() === `bread`){
					temp.push(<ProductCard key={product._id} productProp={product}/>);
				}
			})
			setBreads(temp);
			temp = [];

			// setPies(
			// 	result.map(product=>{
			// 		if(product.category.toLowerCase() === `pies`){
			// 			return <ProductCard key={product._id} productProp={product}/>
			// 		}
			// 	})
			// )

			result.forEach(product=>{
				if(product.category.toLowerCase() === `pies`){
					temp.push(<ProductCard key={product._id} productProp={product}/>);
				}
			})
			setPies(temp);
			temp = [];

			
			result.forEach(product=>{
				if(product.category.toLowerCase() === `cheesecakes`){
					temp.push(<ProductCard key={product._id} productProp={product}/>);
				}
			})
			setCheesecakes(temp);
			temp = [];

			
			result.forEach(product=>{
				if(product.category.toLowerCase() === `cakes`){
					temp.push(<ProductCard key={product._id} productProp={product}/>);
				}
			})
			setCakes(temp);
			temp = [];

			
			result.forEach(product=>{
				if(product.category.toLowerCase() === `quick bites`){
					temp.push(<ProductCard key={product._id} productProp={product}/>);
				}
			})
			setBites(temp);
			temp = [];
		})
	}

	const getTitle = () => {
		switch(category){
			case `breads`: return `BREAD`;
							break;
			case `pies`: return `PIES`;
							break;
			case `bites`: return `QUICK BITES`;
							break;
			case `cakes`: return `CAKES`;
							break;
			case `cheesecakes`: return `CHEESECAKES`;
							break;
			default: return `CAKES`;
		}
	}

	const getDisplay = () => {
		switch(category){
			case `breads`: return breads;
							break;
			case `pies`: return pies;
							break;
			case `bites`: return bites;
							break;
			case `cakes`: return cakes;
							break;
			case `cheesecakes`: return cheesecakes;
									break;
			default: return cakes;
		}
	}

	const handlePageClick = ({selected: selectedPage}) => {
		setCurrentPage(selectedPage);
	}

	const offset = currentPage * itemsPerPage;

	//total pages
	const pageCount = Math.ceil(getDisplay().length / itemsPerPage);

	//slice array to display number of items per page
	const currentPageData = getDisplay().slice(offset, offset + itemsPerPage)

	//display page --------------------------------------------
	return(
		<Container fluid className="welcome" style={{overflowY:'scroll'}}>
			<Row>
				<Col md={2} className="p-0">
					<Sidebar indicator={`products`}/>
				</Col>

				<Col md={10} className="mx-auto my-auto">
					<div style={{height:50}}/>
					<h1 className="my-3">
						<span className="loginLabel">
							{getTitle()}
						</span>
					</h1>
					<div className="d-flex flex-wrap" >
						{
							/*getDisplay()*/
							currentPageData
						}
					</div>
					<center><ReactPaginate
						previousLabel={"<"}
						nextLabel={">"}
						pageCount={pageCount}
						containerClassName="pagination"
						onPageChange={handlePageClick}
						pageClassName={"mx-1 px-2 blackOpaque"}
						nextClassName={"mx-1 px-2 whiteOpaque"}
						previousClassName={"mx-1 px-2 whiteOpaqe"}
						previousLinkClassName={"sideLink"}
						nextLinkClassName={"sideLink"}
						disabledLinkClassName={"mx-1 px-2 disabledPageLink"}
					/></center>
				</Col>
			</Row>
		
		</Container>
	)

}