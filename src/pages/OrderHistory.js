import { useEffect, useState, useContext, Fragment } from 'react';
import { Container, Row, Col, Table, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

import moment from 'moment';
import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
import ReactPaginate from 'react-paginate';


export default function OrderHistory(){

	const [orders, setOrders] = useState("");

	const navigate = useNavigate();
	const { state } = useContext(UserContext);

	//retrieve local storage variables
	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	//variables for pagination
	const [currentPage, setCurrentPage] = useState(0);
	const [itemsPerPage, setItemsPerPage] = useState(0);

	const screenSize = window.innerWidth;

	useEffect(()=>{

		if ( token === null ){
			navigate(`/`);
		} else {
			fetchData();
		}

		if (screenSize <= 1440){
			setItemsPerPage(6);
		} else if (screenSize > 1440 && screenSize < 2560 ){
			setItemsPerPage(10);
		} else {
			setItemsPerPage(16);
		}

	},[])

	const fetchData = () => {

		let fetchAPI = "";

		if ( admin === "true" ){
			fetchAPI = `https://tranquil-refuge-66470.herokuapp.com/api/orders/all-orders`;
		} else {
			fetchAPI = `https://tranquil-refuge-66470.herokuapp.com/api/orders/my-orders`;
		}

		fetch(fetchAPI, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
				
			if (result) {
				setOrders(result.map(order => {
					return(formatData(order))
				}));
			} else {
				setOrders([<tr><td ColSpan="8">No orders</td></tr>]);
			}

		})
	}

	const formatData = (order) => {
		return(
			<tr key={order._id} className="p-1">
				<td className="p-2">{order._id}</td>
				<td className="p-2"><Link to={`./profile/${order.orderedById}`}>{order.orderedBy}</Link></td>
				<td className="p-2">&#8369;{order.totalAmount.toFixed(2)}</td>
				<td className="p-2">{order.paymentStatus}</td>
				<td className="p-2">{order.orderStatus}</td>
				<td className="p-2">{
							order.purchasedOn === undefined ?
							`------`
							: moment(order.purchasedOn).format('MM-DD-YYYY HH:mm')
				}</td>
				<td className="p-1">{	
						order.orderFulfilled === undefined ?
						`------`
						: moment(order.orderFulfilled).format('MM-DD-YYYY HH:mm')
				}</td>
				<td className="p-2">
					<Button className="tableBtn"
						onClick={()=>navigate(`/order-details/${order._id}`)}
					>View</Button>
				</td>
			</tr>
		)
	}

	const orderBackground = () =>{
		if (admin === "true"){
			return "adminPage"
		} else {
			return "welcome"
		}
	}

	const handlePageClick = ({selected: selectedPage}) => {
		setCurrentPage(selectedPage);
	}

	const offset = currentPage * itemsPerPage;

	//total pages
	const pageCount = Math.ceil(orders.length / itemsPerPage);

	//slice array to display number of items per page
	const currentPageData = orders.slice(offset, offset + itemsPerPage)

	return(
		<Container fluid className={orderBackground()} style={{overflowY:'scroll'}}>
			<Row>
				{
					admin === "true" ?
					<Col md={2} className="sideColumn ">
						<Sidebar indicator={`admin`}/>
					</Col>
					: <Fragment></Fragment>
				}
				<Col md={10} className="mx-auto">
					<div style={{height:60}}/>
					<h3><center><span className="loginLabel blackOpaque">ORDER HISTORY</span></center></h3>
					<Table variant="striped" className="tableBackground mt-4">
						<thead>
							<tr className="p-2 tableHeader">
								<th className="p-2">Order Id</th>
								<th className="p-2">Ordered By</th>
								<th className="p-2">Order Amount</th>
								<th className="p-2">Payment Status</th>
								<th className="p-2">Order Status</th>
								<th className="p-2">Order Date</th>
								<th className="p-2">Order Completed Date</th>
								<th className="p-2">Details</th>
							</tr>
						</thead>
						<tbody>
							{
								/*orders*/
								currentPageData
							}
						</tbody>
					</Table>
					<center><ReactPaginate
						previousLabel={"<"}
						nextLabel={">"}
						pageCount={pageCount}
						containerClassName="pagination"
						onPageChange={handlePageClick}
						pageClassName={"mx-1 px-2 blackOpaque"}
						nextClassName={"mx-1 px-2 whiteOpaque"}
						previousClassName={"mx-1 px-2 whiteOpaqe"}
						previousLinkClassName={"sideLink"}
						nextLinkClassName={"sideLink"}
						disabledLinkClassName={"mx-1 px-2 disabledPageLink"}
					/>
					{
						admin === "true" ? <Fragment></Fragment>
						: <Fragment>
							<Button 
								variant="light"
								className="btn tableBtn m-2 d-block"
								onClick={()=> navigate('../order-history')}
							>Back to order list</Button>
							<Button 
								variant="dark"
								className=" btn tableBtn m-2 d-block"
								onClick={()=>navigate('../products')}
							>Back to products</Button>
						</Fragment> 

					}</center>
				</Col>
				<Col xs={12}>
				</Col>
			</Row>
		</Container>
	)
}