import { useParams, useNavigate } from 'react-router-dom';
import {Container, Row, Col, Button, InputGroup, FormControl} from 'react-bootstrap';
import { useEffect, useState, useContext, Fragment } from 'react';
import { Link } from 'react-router-dom';
// import UserContext from '../UserContext';

import Sidebar from '../components/Sidebar';
import ImageCarousel from '../components/ImageCarousel';
// import QuantitySelector from '../components/QuantitySelector';

const admin = localStorage.getItem('admin');
const token = localStorage.getItem('token');

export default function Product(){

	const {productId} = useParams();

	const [productName, setProductName] = useState("");
	const [price, setPrice] = useState("");
	const [description, setDescription] = useState("");
	const [quantity, setQuantity] = useState(1);

	// const { user } = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(()=>{

		console.log(admin === "false");

		if(admin === "true"){
			navigate(`../all-products`);
			
		} else {
			fetchData();
		}
	},[])

	const fetchData = () => {
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/product/${productId}`)
		.then(result => result.json())
		.then(result => {

			//assign values to useState
			setProductName(result.name);
			setPrice(result.price);
			setDescription(result.description);

		});
	}

	//child to parent
	// const getQuantity = (qty) => {
	// 	console.log(`parent get quantity`)

	// 	setQuantity(qty);
		
	// 	console.log(`data from child`, qty);
	// 	console.log(`value parent quantity`, quantity);
	// } 

	const addQty = () => {
		setQuantity(quantity + 1);
		// passValue();
	}

	const decQty = () => {
		if ( quantity > 1 ){
			setQuantity(quantity - 1);
		}
	}

	const addToCart = () => {

		let order = {
			productId: productId,
			productName: productName,
			productQty: quantity,
			productPrice: price,
			subTotal: price * quantity
		};

		if ( localStorage.getItem('orders') !== null){
			let cart = JSON.parse(localStorage.getItem('orders'));

			if (cart.some(item=>item.productId == order.productId)){
				alert(`Product already in cart.`)
			} else {
				cart.push(order);
				localStorage.setItem('orders', JSON.stringify(cart));
				alert('Order added to cart.');
			}
		//add to local storage if not exists
		} else {
			
			let orderArr = [];
			orderArr.push(order);
			localStorage.setItem('orders', JSON.stringify(orderArr));
			alert('Order added to cart.');
		}

	}

	return(
		<Container fluid className="welcome m-0 p-0">
			<Row>
				<Col md={3} className="">
					<Sidebar indicator={`cart`}/>
				</Col>
				<Col md={9} className="p-5">
					<div style={{height:80}}/>
					<Row className="blackOpaque py-5">
						<Col>
							<Row className="mx-auto" fluid>
								<Col md={5} className="mx-0" >
									<ImageCarousel/>
								</Col>
								<Col md={7} className="m-0 p-0 my-auto">
									<Row>
										<Col xs={12}>
											<h1>{productName}</h1>
											<h5>&#8369;{price}</h5>
											<p>{description}</p>
										</Col>
										<Col xs={12}>
											<center>
												{/*add to cart component*/}
												{/*<QuantitySelector getQuantity={getQuantity}/>*/}
												{
													admin === "false" ?
													<Fragment>
														<InputGroup className="mb-3 cartQty" style={{width:130}}>
														  <Button variant="light" onClick={()=>decQty()}>-</Button>
														  <FormControl
														    className="login cartNumberField"
														    type="number"
														    value={quantity}
														    onChange={(e)=> setQuantity(e.target.value)}
														  />
														  <center><Button variant="light" onClick={()=>addQty()}>+</Button></center>
														</InputGroup>
														<Button className="btn btn-danger m-2" onClick={()=>addToCart()}>Add to cart</Button>
													</Fragment>
													: <Fragment></Fragment>
												}
												<Link to={`/products`} className="btn tableBtn m-2">Back to products</Link>
											</center>
										</Col>
									</Row>
								</Col>
							</Row>
						</Col>
					</Row>
				</Col>
			</Row>
		</Container>
	)
}