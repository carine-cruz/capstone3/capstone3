import {useState, useEffect} from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Container, Row, Form, Col, Button, DropdownButton, Dropdown, InputGroup} from 'react-bootstrap';
import ImageCarousel from '../components/ImageCarousel';


export default function NewProduct(){
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [category, setCategory] = useState(`Select category`);
	const [stocks, setStocks] = useState(0);
	// const [isActive, setIsActive] = useState("");

	const {productId} = useParams();

	const navigate = useNavigate();

	if (localStorage.getItem('admin') !== "true"){
		navigate(`../products`);
	}


	const addProduct = (e) =>{
		e.preventDefault();

		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/new-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				stockCount: stocks 
			})
		})
		.then(result => result.json())
		.then(result => {

			if (result){
				alert(`New product created.`);
				navigate(`../all-products/cakes`);
			} else {
				alert(result.message)
			}
		})
	}

	const handleDDown = (e) => {
		setCategory(e);
	}

	return (
		<Container fluid className="adminPage min-vh-100 mx-auto">
			
			<Row className="fluid mx-auto my-auto">
				<Col md={5} className="my-auto">
					<ImageCarousel/>
					<center><Button className="mainButton my-2" variant="outline-dark">Upload Picture</Button></center>
				</Col>
				<Col className="my-auto blackOpaque p-3" md={3}>
					<h3 className="loginLabel">NEW PRODUCT</h3>
					<Form onSubmit={(e) => addProduct(e)}>
					  <Form.Group className="mb-3">
					  	<Form.Control 
					  		type="text" 
					  		className="login" 
					  		value={name}
					  		placeholder="Product name" 
					  		onChange={(e)=> setName(e.target.value)}
					  	/>
					  </Form.Group>

					  <Form.Group className="mb-3">
					  <InputGroup>
					  	<InputGroup.Text>&#8369;</InputGroup.Text>
					    <Form.Control 
					    	type="number" 
					    	className="login" 
					    	value={price}
					    	placeholder="Price"
					    	onChange={(e)=> setPrice(e.target.value)}
					    />
					    </InputGroup>
					    </Form.Group>
					  

					  <Form.Group className="mb-3">
					    <Form.Control 
					    	as="textarea" 
					    	className="login" 
					    	value={description}
					    	placeholder="Description"
					    	onChange={(e)=> setDescription(e.target.value)}
					    	row="3"
					    />
					   
					  </Form.Group>

					  <Form.Group className="mb-3">
							<DropdownButton 
								variant="outline-light" 
								title={category} 
								className="btn-block"
								onSelect={handleDDown} >
							    <Dropdown.Item eventKey="Bread">Bread</Dropdown.Item>
							    <Dropdown.Item eventKey="Pies">Pies</Dropdown.Item>
							    <Dropdown.Item eventKey="Cakes">Cakes</Dropdown.Item>
							    <Dropdown.Item eventKey="Cheesecakes">Cheesecakes</Dropdown.Item>
							    <Dropdown.Item eventKey="Quick Bites">Quick Bites</Dropdown.Item>
							</DropdownButton>
					  </Form.Group>

					  <Form.Group className="mb-3">
					  	<InputGroup>
					  		<InputGroup.Text>Stocks</InputGroup.Text>
					    <Form.Control 
					    	type="number" 
					    	className="login" 
					    	value={stocks}
					    	placeholder="Stocks"
					    	onChange={(e)=> setStocks(e.target.value)}
					    />
					    </InputGroup>
					  </Form.Group>

					  <center>
					  	<Button variant="outline-light" type="submit" className="mainButton">
					    Submit
					  	</Button>
					  	<Button 
					  		variant="outline-light" 
					  		type="submit" 
					  		className="mainButton m-2" 
					  		onClick={()=>navigate(`../all-products/cakes`)}>
					  	  Cancel
					  	</Button>
					  </center>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}