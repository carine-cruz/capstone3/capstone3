import {Navbar, Container, Nav, Form, FormControl, Button, NavDropdown} from 'react-bootstrap'
import {Fragment, useContext} from 'react';
import UserContext from './../UserContext';

import userLogo from '../images/user.png';
import cartLogo from '../images/cart.png';
import productLogo from '../images/product.png';
import homeLogo from '../images/home.png';

export default function AppNavbar(){

	// const { user } = useContext(UserContext);
	const token = localStorage.getItem('token');
	const admin = localStorage.getItem('admin');

	 return(
	 	<Navbar expand="lg" className="m-0 p-0 navbar shadow" fixed="top">
	 		<Container fluid>
	 	    	<Navbar.Brand href="/" className="logo py-0 text-align-left">THE BAKERY</Navbar.Brand>
	 	    	<Navbar.Toggle aria-controls="basic-navbar-nav" />
	 	    	<Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
	 	      		<Nav className="me-auto my-0 mx-2">
	 	      		{
	 	      			token === null ? <Nav.Link href="/" className="navText"><img src={homeLogo} className="img-fluid navbarImg2 "/></Nav.Link>
	 	      			: <Fragment></Fragment>
	 	      		}
	 	        		{
	 	        			admin === "false" ? 
	 	        			<Fragment>
	 	        				<Nav.Link href="../products" className="navText"><img src={productLogo} className="navbarImg2 img-fluid"/></Nav.Link>
	 	        				<Nav.Link href="/cart" className="navText"><img src={cartLogo} className="navbarImg2 img-fluid"/></Nav.Link> 
	 	        			</Fragment>
	 	        			: <Nav.Link href="../all-products" className="navText"><img src={productLogo} className="navbarImg2 img-fluid"/></Nav.Link>
	 	        		}
	 	        		<NavDropdown title={<img src={userLogo} className="navbarImg img-fluid"/>} id="basic-nav-dropdown" className="navText">
	 	          			
	 	          			{
	 	          				token === null 
	 	          				? <Fragment>
	 	          					<NavDropdown.Item href="/login" className="navText">Log-in</NavDropdown.Item>
	 	          					<NavDropdown.Item href="/register" className="navText">Register</NavDropdown.Item>
	 	          				</Fragment>
	 	          				: <Fragment>
	 	          					<NavDropdown.Item href="/order-history" className="navText">Order History</NavDropdown.Item>
	 	          					<NavDropdown.Divider />
	 	          					<NavDropdown.Item href="/logout" className="navText">Log-out</NavDropdown.Item>
	 	          				</Fragment>
	 	          			}
	 	        		</NavDropdown>
	 	      		</Nav>
	 	    	</Navbar.Collapse>
	 	  </Container>
	 	</Navbar>
	)
}