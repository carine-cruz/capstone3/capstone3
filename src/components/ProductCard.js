import {Card, Button, Container, Row, Col} from 'react-bootstrap';
import image from './../images/bread.jpg';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}){
	// console.log(productProp)
	const { _id, name, price } = productProp;

	return (
			<Container className="productCard mx-auto my-3 d-flex p-0">
				<Link to={`/product/${_id}`}>
				<Row>
					<Col md={6}>
						<img src={image} className="img-fluid"/>
					</Col>
					<Col md={6} className="p-3">
						<h5 className="productText">{name}</h5>
						<p className="productText">&#8369;{price.toFixed(2)}</p>
					</Col>
				</Row>
				</Link>
			</Container>
	)
}